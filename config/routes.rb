Rails.application.routes.draw do
  root to: 'home#index'
  namespace :api do
    namespace :v1 do
      resources :comments
      resources :tasks, only: %i[index show create]
    end
  end

  devise_for :admins
  devise_for :users, :controllers => { :omniauth_callbacks => "callbacks" }

  namespace :admin do
    root to: 'dashboard#show'
    resources :admins
    resources :tasks, only: %i[index show update] do
      resources :comments, only: :create
    end
    resources :users
    resource :dashboard, :controller => 'dashboard', only: :show
  end

  resources :tasks do
    resources :comments
  end
  resources :users, :controller => 'users', only: %i[show destroy]
  resource :dashboard, :controller => 'dashboard', only: :show
end
