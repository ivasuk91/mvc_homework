class Admin::AdminsController < ApplicationController
  before_action :set_admin, only: %i[show destroy]

  def index
    @admins = Admin.all
  end

  def create
    @admin = Admin.new(admin_params)
    if @admin.save
      redirect_to admin_admin_path(@admin)
    else
      render action: 'new'
    end
  end

  def destroy
    @admin.destroy
    redirect_to(admin_admins_url)
  end

  private

  def set_admin
    @admin = Admin.find(params[:id])
  end

  def admin_params
    params.require(:admin).permit(:email, :password)
  end
end
