class Admin::CommentsController < ApplicationController
  before_action :find_task

  # def create
  #   current_admin.comments.create!(comment_params)
  #   redirect_to admin_tasks_path
  # end

  # private

  # def comment_params
  #   params.require(:comment).permit(:task_id, :body)
  # end
  def new
    @comment = Comment.new
  end

  def create
    @comment = @task.comments.build(comment_params)
    if @comment.save
      redirect_to admin_task_path(@task), flash: { notice: 'Comment added successfully' }
    else
      redirect_to admin_task_path(@task), flash: { notice: 'Error while commenting' }
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:body, :author_type)
          .merge(author_id: current_admin.id)
  end

  def find_task
    @task = Task.find(params[:task_id])
  end
end
