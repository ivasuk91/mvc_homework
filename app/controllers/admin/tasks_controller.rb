class Admin::TasksController < ApplicationController
  before_action :authenticate_admin!
  before_action :find_task, only: %i[show update]

  def index
    @tasks = Task.all
  end

  def update
    if @task.update(status: converted_status)
      redirect_to admin_tasks_path
      flash[:notice] = 'Task has been updated successfully!'
    end
  end

  private

  def find_task
    @task = Task.find(params[:id])
  end

  def task_params
    params.require(:task).permit(:status)
  end

  def converted_status
    if %w[1 2].include?(task_params[:status])
      task_params[:status].to_i
    else
      task_params[:status]
    end
  end
end
