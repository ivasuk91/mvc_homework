class Admin::UsersController < ApplicationController
  before_action :set_user, only: %i[show destroy]

  def index
    @users = User.all
  end

  def create
    @user = User.create(user_params)
    if @user.persisted?
      redirect_to admin_user_url(@user)
    else
      render 'new'
    end
  end

  def destroy
    @user.destroy!
    redirect_to %i[admin users]
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :password)
  end
end
