class TasksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_task, only: %i[show edit update destroy]

  def new
    @task = Task.new
  end

  def create
    @task = Task.new(task_params)
    if @task.save
      flash[:notice] = 'Task has been created'
      redirect_to task_path(@task)
    else
      render 'new'
    end
  end

  def update
    if @task.update(task_params)
      flash[:notice] = 'Task has been updated successfully!'
      redirect_to task_path(@task)
    else
      flash[:error] = 'Task has not been updated !'
      render 'edit'
    end
  end

  def destroy
    if @task.destroy
      flash[:notice] = 'Task has been deleted successfully!'
    else
      flash[:error] = 'Task has not been deleted'
    end
    redirect_to root_path
  end

  private

  def task_params
    params.require(:task).permit(:title, :body)
          .merge(user: current_user)
  end

  def set_task
    @task = Task.find(params[:id])
  end
end
