class CommentsController < ApplicationController
  before_action :find_task

  def new
    @comment = Comment.new
  end

  def create
    @comment = @task.comments.build(comment_params)
    if @comment.save
      redirect_to task_path(@task), flash: { notice: 'Comment added successfully' }
    else
      redirect_to task_path(@task), flash: { notice: 'Error while commenting' }
    end
  end

  def destroy
    if @comment.destroy
      redirect_to task_path(@task)
    else
      redirect_to task_path(@task), flash: { error: 'Something went wrong' }
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:body, :author_type)
          .merge(author_id: current_user.id)
  end

  def find_task
    @task = Task.find(params[:task_id])
  end
end
