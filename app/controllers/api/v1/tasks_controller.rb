class Api::V1::TasksController < ApplicationController
  respond_to :json

  def index
    @tasks = Task.all
    render json: @tasks
  end

  def show
    render json: @task
  end

  def create
    @task = Task.create(task_params)
    if @task.save
      render status: 200, json: {
        message: 'Successfully created.',
        Task: @task
      }.to_json
    else
      render status: 500, json: {
        errors: @task.errors
      }.to_json
    end
  end

  private

  def task_params
    params.require(:task).permit(:title, :body, :status, :user_id)
  end
end
