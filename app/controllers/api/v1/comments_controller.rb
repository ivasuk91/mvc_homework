class Api::V1::CommentsController < ApplicationController
  respond_to :json

  def create
    @comment = current_user.comments.build(comment_params)

    if @comment.save
      render :show, status: :created
    else
      render json: @comment.errors
    end
  end
end
