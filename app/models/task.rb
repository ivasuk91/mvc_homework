class Task < ApplicationRecord
  belongs_to :user
  belongs_to :admin, optional: true
  has_many :comments
  enum status: %i[opened closed]

  validates :title, presence: true
  validates :body, presence: true
end
