class Admin < ApplicationRecord
  has_many :tasks
  has_many :comments, as: :author

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
