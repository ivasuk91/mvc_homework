class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.text :body
      t.references :task, foreign_key: true
      t.references :author, polymorphic: true

      t.timestamps
    end
  end
end
